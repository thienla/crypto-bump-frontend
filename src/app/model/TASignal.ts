/**
 * Created by truongnhukhang on 6/4/18.
 */
export class TASignal {
  id: string;
  symbol: string;
  closeTime: number;
  bullishSignal: any;
  recentBuyVolume: any;
  recentBuySymbolVolume: any;
  recentSellVolume: any;
  recentSellSymbolVolume: any;
  btcMA5_1d: any;
  btcMA5_4h: any;
  btcSignals: any;
  currentPrice: number;
  priceAfter5min: number;
  priceAfter10min: number;
  priceAfter15min: number;
  priceAfter30min: number;
  priceAfter60min: number;
  priceAfter120min: number;
  priceAfter240min: number;
  priceAfter480min: number;
  priceAfter1440min: number;
  priceAfter2480min: number;
}
