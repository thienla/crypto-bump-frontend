import {CandleStick} from "./candleStick";

export class PumpSignal extends CandleStick{
  rsiTimeFrames: any;
  stochOscTimeFrames: any;
  closeTimeFrames: any;
  histogramTimeFrames: any;
  ma5TimeFrames: any;
  ma9TimeFrames: any;
  ma20TimeFrames: any;
  ma50TimeFrames: any;
  ma100TimeFrames: any;
  recentVolume: any;
  recentBuyVolume: any;
  recentBuySymbolVolume: any;
  recentSellVolume: any;
  recentSellSymbolVolume: any;
  recentTrades: any;
  futurePrices: any;
}
