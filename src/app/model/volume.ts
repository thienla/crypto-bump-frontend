/**
 * Created by truongnhukhang on 4/17/18.
 */
export class Volume {
  symbol: string;
  candlestickInterval: number;
  startTime: number;
  endTime: number;
  startDate: Date;
  endDate: Date;
  numberBTC_In: number;
  numberBTC_out: number;
  buyVolume: Map<string, number>;
  sellVolume: Map<string, number>;
}
