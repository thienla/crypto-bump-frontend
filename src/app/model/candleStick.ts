export class CandleStick {
  symbol: string;
  dateTime: number;
  currentPrice: number;
  volume: number;
  quoteVolume: number;

}
