/**
 * Created by truongnhukhang on 7/26/18.
 */
export class SignalFilterObject {
  signalListByInterval: any;
  scoreByInterval: any;
  currentPrice: any;
  page: number;
  size: number;
}
