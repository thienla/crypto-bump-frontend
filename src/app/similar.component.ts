import {AfterContentChecked, AfterViewInit, Component, OnInit} from "@angular/core";
import {TASignal} from "./model/TASignal";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFirestore} from "angularfire2/firestore";
import {HttpClient} from "@angular/common/http";
import {SignalFilterObject} from "./model/SignalFilterObject";
import {PumpComponent} from "./pump.component";

@Component({
  selector: 'app-root',
  templateUrl: './similar.component.html',
  styleUrls: ['./app.component.css']
})
export class SimilarComponent implements AfterViewInit, OnInit, AfterContentChecked {

  taSignal: TASignal;
  // similarSignals: TASignal[];
  dataSource: any;
  symbol: string;
  loading = false;
  displayedColumns = ['symbol', '1440', '240', '120', '60', '30', 'futurePrice', 'btcPrice', 'btcSignals', 'action'];
  predictionDto : any;
  constructor(
    private route: ActivatedRoute,
    private db: AngularFirestore,
    private http: HttpClient,
    private router: Router
  ) {
  }


  ngAfterContentChecked(): void {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.loading = true;
      this.symbol = params['symbol'];
      this.db.collection('Luna_data').doc(this.symbol).valueChanges().subscribe(value => {
        this.taSignal = <TASignal>value;
        const signalFilterObject = new SignalFilterObject();
        const singalListByInterval = {};
        const scoreByInterval = {};
        singalListByInterval['30'] = this.taSignal.bullishSignal['30'];
        singalListByInterval['60'] = this.taSignal.bullishSignal['60'];
        singalListByInterval['120'] = this.taSignal.bullishSignal['120'];
        singalListByInterval['240'] = this.taSignal.bullishSignal['240'];
        singalListByInterval['1440'] = this.taSignal.bullishSignal['1440'];
        scoreByInterval['30'] = 1;
        scoreByInterval['60'] = 2;
        scoreByInterval['120'] = 3;
        scoreByInterval['240'] = 4;
        scoreByInterval['1440'] = 5;
        signalFilterObject.currentPrice = this.taSignal.currentPrice;
        signalFilterObject.signalListByInterval = singalListByInterval;
        signalFilterObject.scoreByInterval = scoreByInterval;
        signalFilterObject.page = 1;
        signalFilterObject.size = 20;
        this.http.get(PumpComponent.analyzer_url + 'btctrend').subscribe(data => {
          if (data) {
            const ma5_4h = [];
            for (let j = 0; j < data['240'].length; j++) {
              const result = [];
              result[0] = j + '';
              result[1] = data['240'][j] * 1.0;
              ma5_4h.push(result);
            }
            ma5_4h.unshift(['NUM', 'MA5']);

            const chartData_4h = {
              chartType: 'LineChart',
              dataTable: ma5_4h,
              options: {
                chartArea: {'width': '100%', 'height': '95%'},
                legend: 'none',
                height: 100,
                vAxis: {
                  gridlines: {
                    count: 5,
                  },
                  textPosition: 'none',
                },
                hAxis: {
                  textPosition: 'none'
                },
              },
            };
            this.taSignal.btcMA5_4h = chartData_4h;
            const ma5_1d = [];
            for (let j = 0; j < data['1440'].length; j++) {
              const result = [];
              result[0] = j + '';
              result[1] = data['1440'][j] * 1.0;
              ma5_1d.push(result);
            }
            ma5_1d.unshift(['NUM', 'MA5']);

            const chartData_1d = {
              chartType: 'LineChart',
              dataTable: ma5_1d,
              options: {
                chartArea: {'width': '100%', 'height': '95%'},
                legend: 'none',
                height: 100,
                vAxis: {
                  textPosition: 'none',
                },
                hAxis: {
                  textPosition: 'none'
                },
              },
            };
            this.taSignal.btcMA5_1d = chartData_1d;
          }
        });
        this.http.get(PumpComponent.analyzer_url + 'btcsignals').subscribe(data => {
          const btcSignal: any = data;
          this.taSignal.btcSignals = {};
          this.taSignal.btcSignals['1440'] = btcSignal['1440'];
          this.taSignal.btcSignals['240'] = btcSignal['240'];
          this.taSignal.btcSignals['120'] = btcSignal['120'];
          this.taSignal.btcSignals['60'] = btcSignal['60'];
          this.taSignal.btcSignals['30'] = btcSignal['30'];
        })
        this.http.post(PumpComponent.root_url + 'groupcandles', signalFilterObject).subscribe(data => {
            if (data) {
              this.predictionDto = data;
              this.dataSource = this.predictionDto.groupCandleDtos;
              for (let i = 0; i < this.dataSource.length; i++) {
                const element = this.dataSource[i];
                this.http.get(PumpComponent.root_url + 'groupcandle/' + 'BTCUSDT/' + element.closeTime).subscribe(btc => {
                  const btcSignal: any = btc;
                  element.btcSignals = {};
                  element.btcSignals['1440'] = btcSignal.bullishSignal['1440'];
                  element.btcSignals['240'] = btcSignal.bullishSignal['240'];
                  element.btcSignals['120'] = btcSignal.bullishSignal['120'];
                  element.btcSignals['60'] = btcSignal.bullishSignal['60'];
                  element.btcSignals['30'] = btcSignal.bullishSignal['30'];
                });
                if (element.btcMA5_4h) {
                  const ma5_4h = [];
                  for (let j = 0; j < element.btcMA5_4h.length; j++) {
                    const result = [];
                    result[0] = j + '';
                    result[1] = element.btcMA5_4h[j] * 1.0;
                    ma5_4h.push(result);
                  }
                  ma5_4h.unshift(['NUM', 'MA5']);

                  const chartData_4h = {
                    chartType: 'LineChart',
                    dataTable: ma5_4h,
                    options: {
                      chartArea: {'width': '100%', 'height': '95%'},
                      legend: 'none',
                      height: 100,
                      vAxis: {
                        gridlines: {
                          count: 5,
                        },
                        textPosition: 'none',
                      },
                      hAxis: {
                        textPosition: 'none'
                      },
                    },
                  };
                  element.chartData_4h = chartData_4h;

                }

                if (element.btcMA5_1d) {
                  const ma5_1d = [];
                  for (let j = 0; j < element.btcMA5_1d.length; j++) {
                    const result = [];
                    result[0] = j + '';
                    result[1] = element.btcMA5_1d[j] * 1.0;
                    ma5_1d.push(result);
                  }
                  ma5_1d.unshift(['NUM', 'MA5']);

                  const chartData_1d = {
                    chartType: 'LineChart',
                    dataTable: ma5_1d,
                    options: {
                      chartArea: {'width': '100%', 'height': '95%'},
                      legend: 'none',
                      height: 100,
                      vAxis: {
                        textPosition: 'none',
                      },
                      hAxis: {
                        textPosition: 'none'
                      },
                    },
                  };
                  element.chartData_1d = chartData_1d;
                }
                const futurePrice = [];
                futurePrice.push(['0', element.price * 1.0, '0 : ' + element.price]);
                futurePrice.push(['5', element.priceAfter5min * 1.0, '5 : ' + element.priceAfter5min]);
                futurePrice.push(['10', element.priceAfter10min * 1.0, '10 : ' + element.priceAfter10min]);
                futurePrice.push(['15', element.priceAfter15min * 1.0, '15 : ' + element.priceAfter15min]);
                futurePrice.push(['30', element.priceAfter30min * 1.0, '30 : ' + element.priceAfter30min]);
                futurePrice.push(['60', element.priceAfter60min * 1.0, '60 : ' + element.priceAfter60min]);
                futurePrice.push(['120', element.priceAfter120min * 1.0, '120 : ' + element.priceAfter120min]);
                futurePrice.push(['240', element.priceAfter240min * 1.0, '240 : ' + element.priceAfter240min]);
                futurePrice.push(['480', element.priceAfter480min * 1.0, '480 : ' + element.priceAfter480min]);
                futurePrice.push(['1440', element.priceAfter1440min * 1.0, '1440 : ' + element.priceAfter1440min]);
                futurePrice.push(['2480', element.priceAfter2480min * 1.0, '2480 : ' + element.priceAfter2480min]);
                futurePrice.unshift(['MIN', 'PRICE', {type: 'string', role: 'tooltip', p: {html: true}}]);

                const chartData_futurePrice = {
                  chartType: 'LineChart',
                  dataTable: futurePrice,
                  options: {
                    chartArea: {'width': '100%', 'height': '95%'},
                    legend: 'none',
                    height: 100,
                    vAxis: {
                      textPosition: 'none',
                    },
                    hAxis: {
                      textPosition: 'none'
                    },
                  },
                };
                element.chartData_futurePrice = chartData_futurePrice;
              }
              this.loading = false;
            } else {
              this.loading = false;
            }
          },
          error => {
            this.loading = false;
            alert(error.error.message);
          });

      });

    });
  }

  analyze(symbol, dateTime): void {
    this.loading = true;
    this.http.post(PumpComponent.analyzer_url + symbol + '/' + dateTime, null, {responseType: 'text'}).subscribe(data => {
        this.loading = false;
        if (data === 'created') {
          this.router.navigate(['/analyze/' + symbol + '/' + dateTime]);
        }
      },
      error => {
        this.loading = false;
        alert(error.error.message);
      },
    );
  }

  ngAfterViewInit(): void {
  }

}
