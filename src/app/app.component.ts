/**
 * Created by truongnhukhang on 4/18/18.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <nav class="navbar navbar-expand-lg " style="background-color: #3f51b5;color: whitesmoke;margin-bottom: 50px">
      <span class="navbar-brand mb-0 h1">Altcoin Day Trading</span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a routerLink="/pump" routerLinkActive="active" mat-button class="nav-link" style="color: whitesmoke">Pump Page</a>
          </li>
          <li class="nav-item">
            <a routerLink="/signal" routerLinkActive="active" mat-button class="nav-link" style="color: whitesmoke">Signal Page</a>
          </li>
        </ul>
      </div>
    </nav>

    <router-outlet></router-outlet>
  `
})
export class AppComponent {

}
