import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PumpSignal} from './model/pumpSignal';
import {AngularFirestore} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from "@angular/common/http";
import {PumpComponent} from "./pump.component";

@Component({
  selector: 'app-root',
  templateUrl: './analyzer.component.html',
  styleUrls: ['./app.component.css']
})
export class AnalyzerComponent implements OnInit, AfterViewInit {

  canvas: any;
  ctx: any;
  symbol = 'POWRBTC';
  time = 1522962959999;
  pumpSignal: PumpSignal;
  chart_1min: any;
  chart_5min: any;
  chart_15min: any;
  chart_30min: any;
  chart_60min: any;
  chart_120min: any;
  chart_240min: any;
  chart_1440min: any;
  RSI_1min: any;
  RSI_5min: any;
  RSI_15min: any;
  RSI_30min: any;
  RSI_60min: any;
  RSI_120min: any;
  RSI_240min: any;
  RSI_1440min: any;
  MACD_1min: any;
  MACD_5min: any;
  MACD_15min: any;
  MACD_30min: any;
  MACD_60min: any;
  MACD_120min: any;
  MACD_240min: any;
  MACD_1440min: any;
  STOCH_1min: any;
  STOCH_5min: any;
  STOCH_15min: any;
  STOCH_30min: any;
  STOCH_60min: any;
  STOCH_120min: any;
  STOCH_240min: any;
  STOCH_1440min: any;
  VOL_1min: any;
  VOL_5min: any;
  VOL_15min: any;
  VOL_30min: any;
  VOL_60min: any;
  VOL_120min: any;
  VOL_240min: any;
  VOL_1440min: any;
  loading: boolean = false;

  observablePumpSignal: Observable<PumpSignal[]>;
  constructor(
    private route: ActivatedRoute,
    private db: AngularFirestore,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.symbol = params['symbol'];
      this.time = params['time'];
      this.loading = true;
      this.observablePumpSignal = this.db.collection('tini_data', query => query.where('symbol', '==', this.symbol)
        .where('dateTime', '==', this.time * 1.0)).valueChanges().map(item => <PumpSignal[]><any>item);
      this.observablePumpSignal.subscribe(data => {
        this.loading = false;
        if (data) {
          this.pumpSignal = data[0];
          this.buildCandleChart( '1');
          this.buildCandleChart('5');
          this.buildCandleChart('15');
          this.buildCandleChart('30');
          this.buildCandleChart('60');
          this.buildCandleChart('120');
          this.buildCandleChart('240');
          this.buildCandleChart('1440');
          this.buildRSIChart( '1');
          this.buildRSIChart( '5');
          this.buildRSIChart('15');
          this.buildRSIChart('30');
          this.buildRSIChart('60');
          this.buildRSIChart('120');
          this.buildRSIChart('240');
          this.buildRSIChart('1440');
          this.buildMACDChart('1');
          this.buildMACDChart( '5');
          this.buildMACDChart('15');
          this.buildMACDChart('30');
          this.buildMACDChart('60');
          this.buildMACDChart('120');
          this.buildMACDChart('240');
          this.buildMACDChart('1440');
          this.buildStochChart('1');
          this.buildStochChart( '5');
          this.buildStochChart('15');
          this.buildStochChart('30');
          this.buildStochChart('60');
          this.buildStochChart('120');
          this.buildStochChart('240');
          this.buildStochChart('1440');
          this.buildVolumeChart('1');
          this.buildVolumeChart( '5');
          this.buildVolumeChart('15');
          this.buildVolumeChart('30');
          this.buildVolumeChart('60');
          this.buildVolumeChart('120');
          this.buildVolumeChart('240');
          this.buildVolumeChart('1440');
        }
      },
        error => {
          this.loading = false;
          alert(error.error.message);
        },);
    });
  }

  public updateData(): void {
    this.loading = true;
    this.http.put(PumpComponent.analyzer_url + this.pumpSignal.symbol + '/' + this.pumpSignal.dateTime, null, {responseType: 'text'}).subscribe(data => {
        this.loading = false;
        if (data === 'updated') {
          this.router.navigate(['/analyze/' + this.pumpSignal.symbol + '/' + this.pumpSignal.dateTime]);
        }
      },
      error => {
        this.loading = false;
        alert(error.error.message);
      },
    );
  }

  private buildVolumeChart(timeFrame: string) {
    const candles = (<Array<any>>this.pumpSignal.closeTimeFrames[timeFrame]);
    const volumeData = [];
    for ( let i = 0 ; i < candles.length; i++) {
      const candle = candles[i];
      const result = [];
      result[0] = new Date(candle.openTime).toUTCString();
      result[1] = candle.volume * 1.0;
      volumeData.push(result);
    }
    volumeData.unshift(['TIME', 'Volume']);
    const ChartData = {
      chartType: 'ColumnChart',
      dataTable: volumeData,
      options: {
        chartArea: {'top':5,'left':80,'width': '100%', 'height': '95%'},
        legend: 'none',
        height: 120,
        vAxis: {
          gridlines : {
            count : 5,
          },
        },
        hAxis: {
          textPosition: 'none'
        },
      },
    };
    if (timeFrame === '1') {
      this.VOL_1min = ChartData;
    } else if (timeFrame === '5') {
      this.VOL_5min = ChartData;
    }else if (timeFrame === '15') {
      this.VOL_15min = ChartData;
    }else if (timeFrame === '30') {
      this.VOL_30min = ChartData;
    }else if (timeFrame === '60') {
      this.VOL_60min = ChartData;
    }else if (timeFrame === '120') {
      this.VOL_120min = ChartData;
    }else if (timeFrame === '240') {
      this.VOL_240min = ChartData;
    }else if (timeFrame === '1440') {
      this.VOL_1440min = ChartData;
    }
  }

  private buildStochChart(timeFrame: string) {
    const stochs = (<Array<any>>this.pumpSignal.stochOscTimeFrames[timeFrame]).reverse();
    const candles = (<Array<any>>this.pumpSignal.closeTimeFrames[timeFrame]);
    const stochData = [];
    for ( let i = 0 ; i < stochs.length; i++) {
      const candle = candles[i];
      const stoch = stochs[i];
      const result = [];
      result[0] = new Date(candle.openTime).toUTCString();
      result[1] = stoch.kLine * 1.0;
      result[2] = stoch.dLine * 1.0 ;
      stochData.push(result);
    }
    stochData.unshift(['TIME', 'Kline' , 'Dline']);
    const ChartData = {
      chartType: 'LineChart',
      dataTable: stochData,
      options: {
        chartArea: {'top':5,'left':80,'width': '100%', 'height': '95%'},
        legend: 'none',
        height: 160,
        vAxis: {
          gridlines : {
            count : 5,
          },
        },
        hAxis: {
          textPosition: 'none'
        },
      },
    };
    if (timeFrame === '1') {
      this.STOCH_1min = ChartData;
    } else if (timeFrame === '5') {
      this.STOCH_5min = ChartData;
    }else if (timeFrame === '15') {
      this.STOCH_15min = ChartData;
    }else if (timeFrame === '30') {
      this.STOCH_30min = ChartData;
    }else if (timeFrame === '60') {
      this.STOCH_60min = ChartData;
    }else if (timeFrame === '120') {
      this.STOCH_120min = ChartData;
    }else if (timeFrame === '240') {
      this.STOCH_240min = ChartData;
    }else if (timeFrame === '1440') {
      this.STOCH_1440min = ChartData;
    }
  }

  private buildMACDChart(timeFrame: string) {
    const macds = (<Array<any>>this.pumpSignal.histogramTimeFrames[timeFrame]).reverse();
    const candles = (<Array<any>>this.pumpSignal.closeTimeFrames[timeFrame]);
    const mcdData = [];
    for ( let i = 0 ; i < macds.length; i++) {
      const candle = candles[i];
      const macd = macds[i];
      const result = [];
      result[0] = new Date(candle.openTime).toUTCString();
      result[1] = macd.histogram * 1.0;
      result[2] = macd.histogram * 1.0 > 0 ? 'color : blue' : 'color: red';
      result[3] = macd.macdLine * 1.0;
      result[4] = macd.signalLine * 1.0;
      mcdData.push(result);
    }
    mcdData.unshift(['TIME', 'HISTOGRAM', { role: 'style' }, 'MACDLine', 'SignalLine']);
    const ChartData = {
      chartType: 'ComboChart',
      dataTable: mcdData,
      options: {
        chartArea: {'top':5,'left':80,'width': '100%', 'height': '95%'},
        height: 160,
        seriesType: 'bars',
        series: {
          1: {type: 'line', color: 'blue'}, 2: {type: 'line', color: '#d3af15'}
        },
        vAxis : {
          textPosition: 'none'
        },
        hAxis: {
          textPosition: 'none'
        },
        legend: 'none',
      },
    };
    if (timeFrame === '1') {
      this.MACD_1min = ChartData;
    } else if (timeFrame === '5') {
      this.MACD_5min = ChartData;
    }else if (timeFrame === '15') {
      this.MACD_15min = ChartData;
    }else if (timeFrame === '30') {
      this.MACD_30min = ChartData;
    }else if (timeFrame === '60') {
      this.MACD_60min = ChartData;
    }else if (timeFrame === '120') {
      this.MACD_120min = ChartData;
    }else if (timeFrame === '240') {
      this.MACD_240min = ChartData;
    }else if (timeFrame === '1440') {
      this.MACD_1440min = ChartData;
    }
  }

  private buildRSIChart(timeFrame: string) {
    const rsis = (<Array<any>>this.pumpSignal.rsiTimeFrames[timeFrame]).reverse();
    const candles = (<Array<any>>this.pumpSignal.closeTimeFrames[timeFrame]);
    const rsiData = [];
    for ( let i = 0 ; i < rsis.length; i++) {
      const candle = candles[i];
      const rsi = rsis[i];
      const result = [];
      result[0] = new Date(candle.openTime).toUTCString();
      result[1] = rsi * 1.0;
      rsiData.push(result);
    }
    rsiData.unshift(['TIME', 'RSI']);
    const ChartData = {
      chartType: 'LineChart',
      dataTable: rsiData,
      options: {
        chartArea: {'top':5,'left':80,'width': '100%', 'height': '95%'},
        legend: 'none',
        height: 160,
        vAxis: {
          gridlines : {
            count : 5,
          },
          minValue: 0,
        },
        hAxis: {
          textPosition: 'none'
        },
      },
    };
    if (timeFrame === '1') {
      this.RSI_1min = ChartData;
    } else if (timeFrame === '5') {
      this.RSI_5min = ChartData;
    }else if (timeFrame === '15') {
      this.RSI_15min = ChartData;
    }else if (timeFrame === '30') {
      this.RSI_30min = ChartData;
    }else if (timeFrame === '60') {
      this.RSI_60min = ChartData;
    }else if (timeFrame === '120') {
      this.RSI_120min = ChartData;
    }else if (timeFrame === '240') {
      this.RSI_240min = ChartData;
    }else if (timeFrame === '1440') {
      this.RSI_1440min = ChartData;
    }
  }

  private buildCandleChart(timeFrame: string) {
    const ma5TimeFrame = (<Array<any>>this.pumpSignal.ma5TimeFrames[timeFrame]).reverse();
    const ma9TimeFrame = (<Array<any>>this.pumpSignal.ma9TimeFrames[timeFrame]).reverse();
    const ma20TimeFrame = (<Array<any>>this.pumpSignal.ma20TimeFrames[timeFrame]).reverse();
    const closeTimeFrameData = [];
    for ( let i = 0 ; i < (<Array<any>>this.pumpSignal.closeTimeFrames[timeFrame]).length; i++) {
      const value = (<Array<any>>this.pumpSignal.closeTimeFrames[timeFrame])[i];
      const candle = [];
      candle[0] = new Date(value.openTime).toTimeString();
      candle[1] = value.low * 1.0;
      candle[2] = value.open * 1.0;
      candle[3] = value.close * 1.0;
      candle[4] = value.high * 1.0;
      candle[5] = this.generateStickChartTooltip(new Date(value.openTime).toUTCString(), candle[3], ma5TimeFrame[i], ma9TimeFrame[i], ma20TimeFrame[i]);
      candle[6] = ma5TimeFrame[i] * 1.0;
      candle[7] = this.generateStickChartTooltip(new Date(value.openTime).toUTCString(), candle[3], ma5TimeFrame[i], ma9TimeFrame[i], ma20TimeFrame[i]);
      candle[8] = ma9TimeFrame[i] * 1.0;
      candle[9] = this.generateStickChartTooltip(new Date(value.openTime).toUTCString(), candle[3], ma5TimeFrame[i], ma9TimeFrame[i], ma20TimeFrame[i]);
      candle[10] = ma20TimeFrame[i] * 1.0;
      candle[11] = this.generateStickChartTooltip(new Date(value.openTime).toUTCString(), candle[3], ma5TimeFrame[i], ma9TimeFrame[i], ma20TimeFrame[i]);

      closeTimeFrameData.push(candle);
    }
    closeTimeFrameData.unshift(['time', 'low', 'open', 'close', 'high', {type: 'string', role: 'tooltip', p: {html: true}}, 'ma5', {type: 'string', role: 'tooltip', p: {html: true}}, 'ma9', {type: 'string', role: 'tooltip', p: {html: true}},  'ma20', {type: 'string', role: 'tooltip', p: {html: true}}]);
    const ChartData = {
      chartType: 'ComboChart',
      dataTable: closeTimeFrameData,
      options: {
        chartArea: {'top':5,'left':80,'width': '100%', 'height': '95%'},
        height: 300,
        tooltip: {isHtml: true},
        seriesType: 'candlesticks',
        series: {
          1: {type: 'line', color: 'blue'}, 2: {type: 'line', color: 'red'}, 3: {type: 'line'}
        },
        legend: 'none',
        vAxis: {
          gridlines : {
            count : 10,
          },
        },
        hAxis: {
          textPosition: 'none'
        },
        candlestick: {
          fallingColor: {strokeWidth: 0, fill: '#a52714'}, // red
          risingColor: {strokeWidth: 0, fill: '#0f9d58'}   // green
        }
      },
    };
    if (timeFrame === '1') {
      this.chart_1min = ChartData;
    } else if (timeFrame === '5') {
      this.chart_5min = ChartData;
    }else if (timeFrame === '15') {
      this.chart_15min = ChartData;
    }else if (timeFrame === '30') {
      this.chart_30min = ChartData;
    }else if (timeFrame === '60') {
      this.chart_60min = ChartData;
    }else if (timeFrame === '120') {
      this.chart_120min = ChartData;
    }else if (timeFrame === '240') {
      this.chart_240min = ChartData;
    }else if (timeFrame === '1440') {
      this.chart_1440min = ChartData;
    }

  }

  ngAfterViewInit(): void {

  }

  public generateStickChartTooltip(time, close, ma5, ma9, ma20): string {
    return "<table>" +
      "<tr>" +
      "<th>Time</th>" +
      "<th>" + time + "</th>" +
    "</tr>" +
    "<tr>" +
      "<td>Close</td>" +
    "<td>" + close + "</td>" +
    "</tr>" +
    "<tr>" +
    "<td>MA5</td>" +
    "<td>" + ma5 + "</td>" +
    "</tr>" +
    "<tr>" +
    "<td>MA9</td>" +
    "<td>" + ma9 * 1.0 + "</td>" +
    "</tr>" +
    "<tr>" +
    "<td>MA20</td>" +
    "<td>" + ma20 + "</td>" +
    "</tr>" +
    "</table>";
  }

}
