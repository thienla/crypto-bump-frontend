import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import { PumpComponent } from './pump.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {MatTableModule, MatInputModule, MatNativeDateModule, MatSortModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {AnalyzerComponent} from './analyzer.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import {AppComponent} from "./app.component";
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import {MatButtonModule} from '@angular/material/button';
import {SignalComponent} from "./signal.component";
import {MatChipsModule} from '@angular/material/chips';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import {SimilarComponent} from "./similar.component";
import {MatCheckboxModule} from '@angular/material/checkbox';

const appRoutes: Routes = [
  { path: 'pump', component: PumpComponent },
  { path: '', component: PumpComponent },
  { path: 'signal', component: SignalComponent },
  { path: 'similar/:symbol', component: SimilarComponent },
  { path: 'analyze/:symbol/:time', component: AnalyzerComponent },
];
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes, { enableTracing: true }
    ),
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff',
      secondaryColour: '#ffffff',
      fullScreenBackdrop: true,
      tertiaryColour: '#ffffff'
    }),
    HttpClientModule,
    BrowserModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatSortModule,
    FormsModule,
    MatInputModule,
    LoadingModule,
    MatDatepickerModule,
    Ng2GoogleChartsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatButtonModule,
    MatChipsModule,
    MatTooltipModule,
    MatCardModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'my-app-name'), // imports firebase/app needed for everything
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
  ],
  declarations: [ AppComponent, PumpComponent , AnalyzerComponent, SignalComponent, SimilarComponent],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
