import {AfterContentChecked, AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Rx';
import {CandleStick} from './model/candleStick';
import {MatSort, MatTableDataSource} from '@angular/material';
import * as Push from 'push.js';
import {Subscription} from 'rxjs/Subscription';
import {PumpSignal} from './model/pumpSignal';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './pump.component.html',
  styleUrls: ['./app.component.css']
})
export class PumpComponent implements AfterViewInit, OnInit , AfterContentChecked {


  public static analyzer_url = 'https://dailyaltcoin.trade/api/analyze/';
  // public static analyzer_url = 'http://localhost:8080/api/analyze/';
  public static root_url = 'https://dailyaltcoin.trade/';
  // public static root_url = 'http://localhost:8080/';
  observableCandleStick: Observable<CandleStick[]>;
  subcriptionCandleStick: Subscription;
  displayedColumns = ['symbol', 'dateTime', 'currentPrice', 'volume' , 'quoteVolume', 'action'];
  candles: CandleStick[];
  dataSource: any;
  alreadyInit = false;
  startDate: Date;
  endDate: Date;
  loading: boolean = false;
  constructor(private db: AngularFirestore, private http: HttpClient, private router: Router) {

  }

  @ViewChild(MatSort) sort: MatSort;

  /**
   * Set the sort after the view init since this component will
   * be able to query its view for the initialized sort.
   */
  ngOnInit(): void {
    Push.create('Hello world!', {
      body: 'Hello, i will bark when bump',
      icon: '/assets/image/barking-dog-64.png',
      timeout: 4000,
      onClick: function () {
        window.focus();
        this.close();
      }
    });
    this.getDefaultdata();
  }

  private getDefaultdata() {
    if (this.subcriptionCandleStick) {
      this.subcriptionCandleStick.unsubscribe();
    }
    this.observableCandleStick = this.db.collection('kubi_data', ref => ref.orderBy('dateTime', 'desc')
      .limit(150)).valueChanges().map(item => <CandleStick[]><any>item);
    this.subcriptionCandleStick = this.observableCandleStick.subscribe(data => {
      if (this.alreadyInit) {
        const newData = data.filter(item => {
          for (let i = 0; i < this.candles.length; i++) {
            const key = this.candles[i].dateTime + '-' + this.candles[i].symbol + '-' + this.candles[i].currentPrice;
            const itemKey = item.dateTime + '-' + item.symbol + '-' + item.currentPrice;
            if (key === itemKey) {
              return false;
            }
          }
          return true;
        });
        newData.forEach(item =>
          Push.create('Bump !!!', {
            body: item.symbol + '-' + new Date(item.dateTime),
            icon: '/assets/image/barking-dog-64.png',
            timeout: 10000,
            onClick: function () {
              window.focus();
              this.close();
            }
          })
        );
      } else {
        this.alreadyInit = true;
      }
      this.candles = data;
      this.dataSource = new MatTableDataSource(this.candles);

    });
  }
  ngAfterViewInit() {
  }
  ngAfterContentChecked(): void {
    if (this.dataSource) {
      this.dataSource.sort = this.sort;
    }
  }

  analyze(symbol, dateTime): void {
    this.loading = true;
    this.http.post(PumpComponent.analyzer_url + symbol + '/' + dateTime, null, {responseType: 'text'}).subscribe(data => {
        this.loading = false;
        if (data === 'created') {
          this.router.navigate(['/analyze/' + symbol + '/' + dateTime]);
        }
      },
      error => {
        this.loading = false;
        alert(error.error.message);
      },
    );
  }

  filterByDate() {
    // this.subcriptionCandleStick.unsubscribe();
    // this.observableCandleStick = this.db.collection('kubi_data', ref => ref.where('symbol', '==', filterValue).orderBy('dateTime', 'desc')
    //   .limit(150)).valueChanges().map(item => <CandleStick[]><any>item);
    // this.observableCandleStick.subscribe(data => {
    //   this.candles = data;
    //   this.dataSource = new MatTableDataSource(this.candles);
    //
    // });
  }


  applyFilter(filterValue: string) {
    if (filterValue) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toUpperCase(); // MatTableDataSource defaults to lowercase matches
      if (this.subcriptionCandleStick) {
        this.subcriptionCandleStick.unsubscribe();
      }
      this.observableCandleStick = this.db.collection('kubi_data', ref => ref.where('symbol', '==', filterValue).orderBy('dateTime', 'desc')
        .limit(150)).valueChanges().map(item => <CandleStick[]><any>item);
      this.observableCandleStick.subscribe(data => {
        this.candles = data;
        this.dataSource = new MatTableDataSource(this.candles);

      });
    } else if (filterValue === '') {
      this.alreadyInit = false;
      this.getDefaultdata();

    }
  }

}
