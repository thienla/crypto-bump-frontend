// /**
//  * Created by truongnhukhang on 4/17/18.
//  */
// import { Component, AfterViewInit } from '@angular/core';
// import * as Chart from 'chart.js';
// import {Observable} from 'rxjs/Observable';
// import { HttpClient } from '@angular/common/http';
// import {Volume} from './model/volume';
// import { FormControl } from '@angular/forms';
// import { NgModel } from '@angular/forms';
//
//
// const chartColors = {
//   red: 'rgb(255, 99, 132)',
//   orange: 'rgb(255, 159, 64)',
//   yellow: 'rgb(255, 205, 86)',
//   green: 'rgb(75, 192, 192)',
//   blue: 'rgb(54, 162, 235)',
//   purple: 'rgb(153, 102, 255)',
//   grey: 'rgb(201, 203, 207)'
// };
// @Component({
//   selector: 'app-root',
//   templateUrl: './volume.component.html',
//   styleUrls: ['./app.component.css']
// })
// export class VolumeComponent implements AfterViewInit {
//   canvas: any;
//   ctx: any;
//   jsonData: Volume[];
//   candelInterval: number;
//   startDate: Date;
//   endDate: Date;
//   myChart: Chart;
//   constructor(private http: HttpClient) {
//   }
//
//   ngAfterViewInit() {
//
//
//   }
//
//   public getJSON(): Observable<any> {
//     return this.http.get('/assets/data/data_vib.json')
//       .map((res: any) => res.json());
//
//   }
//
//   public analyze() {
//     this.getJSON().subscribe(data => {
//       if (this.myChart != null) {
//         this.myChart.destroy();
//       }
//       console.log(this.startDate.getTime());
//       console.log(this.endDate.getTime());
//       this.jsonData = data.filter(vol => vol.startTime >= this.startDate.getTime()
//       && vol.endTime <= this.endDate.getTime());
//       this.canvas = document.getElementById('myChart');
//       this.ctx = this.canvas.getContext('2d');
//       const labels = this.jsonData.map(vol => new Date(vol.startTime).toString());
//       const sellVolume = this.jsonData.map(vol => vol.numberBTC_out);
//       const buyVolume = this.jsonData.map(vol => vol.numberBTC_In);
//       this.myChart = new Chart(this.ctx, {
//         type: 'line',
//         data: {
//           labels: labels,
//           datasets: [ {
//             label: 'BTC in Volume',
//             fill: false,
//             backgroundColor: chartColors.green,
//             borderColor: chartColors.green,
//             data: buyVolume,
//           },
//             {
//               label: 'BTC out Volume',
//               backgroundColor: chartColors.red,
//               borderColor: chartColors.red,
//               data: sellVolume,
//               fill: false,
//             }
//           ]
//         },
//         options: {
//           responsive: true,
//           title: {
//             display: true,
//             text: 'BTC In And BTC out Chart'
//           },
//           tooltips: {
//             mode: 'index',
//             intersect: false,
//           },
//           hover: {
//             mode: 'nearest',
//             intersect: true
//           },
//           scales: {
//             xAxes: [{
//               display: true,
//               scaleLabel: {
//                 display: true,
//                 labelString: 'Date'
//               }
//             }],
//             yAxes: [{
//               display: true,
//               type: 'linear',
//               scaleLabel: {
//                 display: true,
//                 labelString: 'Value'
//               }
//             }]
//           }
//         }
//       });
//     });
//
//   }
//
// }
