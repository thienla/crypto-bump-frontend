import {AfterContentChecked, AfterViewInit, Component, OnInit, ViewChild} from "@angular/core";
import {MatSort, MatTableDataSource} from "@angular/material";
import {Observable} from "rxjs/Observable";
import {TASignal} from "./model/TASignal";
import {AngularFirestore} from "angularfire2/firestore";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import { map } from 'rxjs/operators';
/**
 * Created by truongnhukhang on 6/4/18.
 */
@Component({
  selector: 'app-root',
  templateUrl: './signal.component.html',
  styleUrls: ['./app.component.css']
})
export class SignalComponent implements AfterViewInit, OnInit , AfterContentChecked {

  displayedColumns = ['symbolSignal', '1440', '240', '120' , '60', '30','futurePrice','lastUpdateTime', 'BuySellBTCRate', 'BuySellRate','action'];
  @ViewChild(MatSort) sort: MatSort;
  observableTASignal: Observable<TASignal[]>;
  tASignals = [];
  dataSource: any;
  showCanPredictOnly : boolean = true;
  constructor(private db: AngularFirestore, private http: HttpClient, private router: Router) {

  }
  ngAfterContentChecked(): void {
  }

  ngOnInit(): void {
    this.observableTASignal = this.db.collection('Luna_data').stateChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as TASignal;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
    this.observableTASignal.subscribe(data => {

      if (this.tASignals.length === 0) {
        this.tASignals = data;
      } else {
        let finded = false;
        for (let i = 0 ; i < this.tASignals.length; i++) {
          if ((<TASignal[]>data)[0].id === this.tASignals[i].id) {
              this.tASignals[i] = data[0];
              finded = true;
              break;
          }
        }
        if (!finded) {
          this.tASignals.push(data[0]);
        }
      }
      this.refreshTable();
    });
  }

  public refreshTable() {
    let filterData = [];
    if(this.showCanPredictOnly) {
      for(let i = 0 ; i < this.tASignals.length;i++) {
        if(this.tASignals[i].priceAfter5min!=0) {
          filterData.push(this.tASignals[i]);
        }
      }
    } else {
      filterData = this.tASignals;
    }
    this.dataSource = new MatTableDataSource(filterData);
  }


  ngAfterViewInit(): void {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  analyze(element: TASignal) {

  }
}
