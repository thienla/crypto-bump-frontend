// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB9-E5pS7NaSAC5W-5yhp9Q0yXNlgJmImo",
    authDomain: "tangential-box-227804.firebaseapp.com",
    databaseURL: "https://tangential-box-227804.firebaseio.com",
    projectId: "tangential-box-227804",
    storageBucket: "tangential-box-227804.appspot.com",
    messagingSenderId: "762520345027"
  }
};
